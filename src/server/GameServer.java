package server;

import javax.xml.ws.Endpoint;

/**
 * Created by julioalfredo on 5/31/16.
 */

public class GameServer {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8234/WebServices/jogo", new GameLogic());
    }
}
